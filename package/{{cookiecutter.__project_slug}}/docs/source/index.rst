.. include:: readme.rst

Guides
------

.. toctree::
   :maxdepth: 1

   dev_guide

Library Reference
-----------------

.. toctree::
   :maxdepth: 1

.. include:: license.rst
