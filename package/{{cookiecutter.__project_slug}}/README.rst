
.. image:: https://badge.fury.io/py/{{ cookiecutter.__package_name }}.svg
    :target: https://pypi.org/project/{{ cookiecutter.__package_name }}
    :alt: Latest package version

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
   :alt: Code style - Black

.. raw:: html

   <hr>

.. list-table::
   :align: left

   * - `Python <https://www.python.org>`_
     - >={{ cookiecutter.__min_version }}

Core services and classes for web RPC server.
See `documentation <https://{{ cookiecutter.__package_name }}.readthedocs.io/>`_ for more info.

Installation
------------

.. code-block:: console

  pip install {{ cookiecutter.__package_name }}
