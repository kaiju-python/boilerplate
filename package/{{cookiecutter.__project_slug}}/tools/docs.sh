#!/usr/bin/env sh

# Build API documentation locally using Sphinx
#
# Resulting documentation will be stored at `docs/build/html/`.
#
# Example:
#
#   tools/docs.sh
#

set -e

main() {

  CUR_DIR="${PWD}"

  echo 'Building documentation locally ...'

  cd docs
  pip install -r requirements.txt || true
  rm -r build/html || true
  make html
  cd ..

  echo 'Documentation index:'
  echo "file://${CUR_DIR}/docs/build/html/index.html"
  echo 'OK'

}

main
