Readme
------

.. list-table::
   :align: left

   * - `Python <https://www.python.org>`_
     - >=3.11

Usage
_____

.. code-block:: console

  pip install cookiecutter
  cookiecutter https://gitlab.com/kaiju-python/boilerplate --directory package

Parameters
__________

- **project_name** - readable name of the project
- **author** - author name
- **email** - author email
- **description** - project short description
